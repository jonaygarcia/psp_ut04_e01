# SSH

## ¿Qué es SSH?

__SSH__ o _Secure Shell_, es un protocolo de administración remota que permite a los usuarios controlar y modificar sus servidores remotos a través de Internet. El servicio se creó como un reemplazo seguro para el Telnet sin cifrar y utiliza técnicas criptográficas para garantizar que todas las comunicaciones hacia y desde el servidor remoto sucedan de manera encriptada. Proporciona un mecanismo para autenticar un usuario remoto, transferir entradas desde el cliente al host y retransmitir la salida de vuelta al cliente.

La imagen de abajo muestra una ventana típica de SSH. Cualquier usuario de Linux o macOS puede hacer SSH en su servidor remoto directamente desde la ventana del terminal. Los usuarios de Windows pueden aprovechar los clientes SSH como Putty. Puede ejecutar comandos shell de la misma manera que lo haría si estuviera operando físicamente el equipo remoto.

![img_01][img_01]

## ¿Cómo funciona SSH?

En Sistemas Operativos _Linux_ o _Mac_, usar SSH para conectarse a un equipo remoto es muy simple, para ello debemos abrir una terminal y ejecutar el siguiente comando:

```bash
ssh user@host
```

El comando anterior le indica a al sistema que desea abrir una conexión Shell segura y cifrada:

* __{user}__ representa la cuenta a la que deses acceder. Por ejemplo, puede que desees acceder al usuario root, que es básicamente para el administrador del sistema con derechos completos para modificar cualquier cosa en el sistema.
* __{host}__ hace referencia al equipo al que se quiere conectar. Puede ser tanto una dirección IP (por ejemplo, 244.235.23.19) o un nombre de dominio (por ejemplo, www.xyzdomain.com).

En Sistemas Operativos _Windows_, deberá utilizarse un cliente SSH para abrir conexiones SSH. Los clientes SSH más populares son [PuTTY](http://www.putty.org/) y [MobaXterm](https://mobaxterm.mobatek.net/).

## Versiones

El objetivo de la versión 1 del protocolo (SSH1), propuesta en 1995, ofrecía una alternativa a las sesiones interactivas (shells) tales como Telnet, rsh, rlogin y rexec. Sin embargo, este protocolo tenía un punto débil que permitía a los hackers introducir datos en los flujos cifrados. Por este motivo, en 1997 se propuso la versión 2 del protocolo (SSH2) como un anteproyecto del IETF. Se puede acceder a los documentos que definen este protocolo en [http://www.ietf.org/html.charters/secsh-charter.html](http://www.ietf.org/html.charters/secsh-charter.html).

_Secure Shell Versión 2_ también incluye un protocolo __SFTP__ (_Secure File Transfer Protocol_; en castellano, _Protocolo Seguro de Transferencia de Archivos_).

## Cómo funciona SSH

Una conexión SSH se establece en varias fases:

* En primera instancia, se determina la identidad entre el servidor y el cliente para establecer un canal seguro (capa segura de transporte).
* En segunda instancia, el cliente inicia sesión en el servidor.

![img_02][img_02]

## Establecer un canal seguro

El establecimiento de una capa segura de transporte comienza con la fase de negociación entre el cliente y el servidor para ponerse de acuerdo en los métodos de cifrado que quieren utilizar. El protocolo SSH está diseñado para trabajar con un gran número de algoritmos de cifrado, por esto, tanto el cliente como el servidor deben intercambiar primero los algoritmos que admiten.

Después, para establecer una conexión segura, el servidor envía al cliente su clave de host. El cliente genera una clave de sesión de 256 bits que cifra con la clave pública del servidor y luego la envía al servidor junto con el algoritmo utilizado. El servidor descifra la clave de sesión con su clave privada y envía al cliente un mensaje de confirmación cifrado con la clave se sesión. Después de esto, las comunicaciones restantes se cifran gracias a un algoritmo de cifrado simétrico, mediante la clave de sesión compartida entre el cliente y el servidor.

La seguridad de la transacción se basa en la confianza del cliente y el servidor en que las claves host de cada una de las partes son válidas. Así, cuando se conecta por primera vez con el servidor, el cliente muestra generalmente un mensaje en el que le pide que acepte la comunicación (y posiblemente le presenta un hash de la clave host del servidor):

```bash
Host key not found from the list of known hosts. Are you sure you want to continue connecting (yes/no)?
```

Para obtener una sesión segura propiamente dicha, es mejor pedirle directamente al administrador del servidor que valide la clave pública presentada. Si el usuario valida la conexión, el cliente guarda la clave host del servidor para evitar tener que repetir esta fase.

Por el contrario, dependiendo de su configuración, el servidor puede, a veces, verificar que el cliente es quien dice ser. Si el servidor tiene un lista de hosts autorizados para la conexión, cifrará el mensaje utilizando la clave pública del cliente (que se encuentra en la base de datos de claves del host) para verificar si el cliente es capaz de descifrarla con su clave privada (esto se llama challenge.

## Autentificación

Una vez que se ha establecido la conexión segura entre el cliente y le servidor, el cliente debe conectarse al servidor para obtener un derecho de acceso. Existen diversos métodos:

* El método más conocido es la contraseña tradicional. El cliente envía un nombre de acceso y una contraseña al servidor a través de la conexión segura y el servidor verifica que el usuario en cuestión tiene acceso al equipo y que la contraseña suministrada es válida.

* Un método menos conocido, pero más flexible, es el uso de claves públicas. Si el cliente elige la clave de autentificación, el servidor creará un challenge y le dará acceso al cliente si éste es capaz de descifrar el challenge con su clave privada.

## Librería JSch

__JSch__ es una implementación java de SSH2. Permite conectarse a un servidor SSH remoto y usar port forwarding, X11 forwarding, transferencias de ficheros, ...

Con la librería _JSch_ podemos implementar todas las funcionalidades de SSH2 en nuestras aplicaciones Java.

Para utilizarla en nuestro proyecto, debemos añadir la siguiente dependencia a nuestro proyecto en el fichero _pom.xml_:

```xml
<dependency>
    <groupId>com.jcraft</groupId>
    <artifactId>jsch</artifactId>
    <version>0.1.54</version>
</dependency>
```


Una vez integrado en nuestro, descargamos la dependencia:

```bash
mvn clean install
```

## Uso de la librería JSch

Suponemos que tenemos una máquina virtual en Virtualbox que tiene la siguiente configuración:

* Usuario/Password para acceder a la máquina virtual: cfgs/cfgs.
* Servidor SSH instalado que escucha en el puerto 22.
* Tarjeta de red configurada en modo _NAT_.
* Tarjeta de red tiene un _Port Forwading_ configurado como PORT HOST: 2222 -> PORT GUEST: 22.

En este caso para acceder a la máquina con el usuario _cfgs_ deberemos usar el comando:

```bash
ssh -p 2222 cfgs@localhost
```

### Acceso SSH utilizando JSch

En este ejemplo nos conectamos de manera remota a la máquina, ejecutamos el comando _ls -ltr_ y mostramos el contenido por pantalla:

```java
import java.io.InputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SSHConnect {

	public static void main(String[] args) {
		String host = "localhost";
		Integer port = 2222;
		String user = "cfgs";
		String password = "cfgs";
		String command1 = "ls -ltr";

		try {

			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: " + channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}

			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
```

## Subir ficheros mediante SFTP utilizando

En este ejemplo subimos un fichero, en este caso _/Volumes/Data/pruebas.txt_ a la carpeta personal del usuario _cfgs_, que tiene la ruta _/home/cfgs_:

> __Nota__: Si tu Sistema Operativo es Windows, tendrás que poner una ruta como _C:\pruebas.txt_. Asegúrate de poner la ruta correcta.

```java
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SFTPConnect {

	public static void main(String[] args) throws Exception {

		String host = "localhost";
		Integer port = 2222;
		String user = "cfgs";
		String password = "cfgs";
		String archivo = "/Volumes/Data/pruebas.txt";

		try {

			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");
			sftp.connect();
			sftp.cd("/home/cfgs");
			System.out.println("Subiendo pruebas.txt ...");
			sftp.put(archivo, "pruebas.txt");

			System.out.println("Archivo subido.");

			sftp.exit();
			sftp.disconnect();

			session.disconnect();
			System.out.println("Disconnected");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
```

## Securizar conexiones no seguras mediante Túneles SSH

En este ejemplo vamos a utilizar una conexión SSH (22) para conectarnos a un servidor MySQL (3306). La idea es dejar oculto el puerto tcp3306 en el Servidor Mysql, y solo permitirnos conexión por SSH.

El cliente al establecer la conexión SSH contra el servidor, va a abrir automáticamente en el host local (Cliente) el puerto 3306 para el mysql, pero de una forma en la que el cliente Mysql local, como mysql-client por línea de comandos, o MySQL Workbench, o cualquier otro, se conecten al server remoto apuntándole a la dirección ip local 127.0.0.1.

Todo el tráfico Mysql dirigido a la ip local de localhost será reenviado, automáticamente, al servidor remoto, por medio del túnel cifrado de SSH, así el único acceso al servidor remoto será por SSH.

El tráfico MySQL en sí, será cifrado en el cliente, enviado por dentro del túnel de red de SSH, descifrado en el servidor y entregado localmente al server en su puerto tcp 3306 local.


```bash
ssh -p 2222 -L 23307:localhost:3306 cfgs@localhost -N
```

* -L permite especificar un puerto local que será reenviado al servidor SSH remoto hacia un puerto remoto.
* 23307: es el puerto local que reenviaremos al servidor remoto.
* localhost: es la dirección ip local en el equipo remoto en la que atiende el servicio. En este caso es la de localhost.
* 3306: es el puerto remoto que será mapeado y asociado a esta conexión en el puerto local.
* cfgs: es el usuario con el que vamos a conectarnos al SSH remoto.
* localhost: es el servidor remoto que atiende en el puerto de SSH hacia Internet, y que localmente tiene un servidor MySQL escuchando en una ip privada (es localhost porque usamos una VM en Virtualbox con un Adaptador de Red tipo NAT).
* 2222: puerto SSH en el que escucha el servidor remoto.
* -N: Justo después de hacer el login, no nos aparecerá un Prompt de Shell.

````bash
netstat -tapn |  grep mysql
// tcp    0    0 127.0.0.1:3306     0.0.0.0:*    LISTEN      18469/mysqld
```

Ahora para conectarnos a nuestro servidor MySQL deberíamos hacerlo a la dirección _localhost:23307_.

[img_01]: img/01.png "SSH Login"
[img_02]: img/02.png "SSH Login"
