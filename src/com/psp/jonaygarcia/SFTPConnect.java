package com.psp.jonaygarcia;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SFTPConnect {

	public static void main(String[] args) throws Exception {

		String host = "localhost";
		Integer port = 2222;
		String user = "cfgs";
		String password = "cfgs";
		String archivo = "/Volumes/Data/pruebas.txt";
		
		try {

			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");
			sftp.connect();
			sftp.cd("/home/cfgs");
			System.out.println("Subiendo pruebas.txt ...");
			sftp.put(archivo, "pruebas.txt");

			System.out.println("Archivo subido.");

			sftp.exit();
			sftp.disconnect();

			session.disconnect();
			System.out.println("Disconnected");
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
